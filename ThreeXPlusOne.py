import matplotlib.pyplot as plt


def iterate(start, end):
    plt.title('Plotting 3x+1 problem')
    for i in range(start, end + 1):
        if i <= 1:
            print("Start must be bigger than 1")
            exit()
        current = i
        counter = 0
        x = [counter]
        y = [current]
        while current != 1:
            if current % 2 == 1:
                current = current * 3 + 1
            else:
                current /= 2
            counter += 1
            x.append(int(counter))
            y.append(int(current))
        plt.plot(x, y, label=i)
        # print(i, x, y)
    plt.legend()
    plt.show()


class ThreeXPlusOne:
    def __init__(self, start, end):
        print("Welcome to 3x+1 visualization.")
        iterate(start, end)


test = ThreeXPlusOne(2, 30)
